using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HelperSettings", menuName = "ProcGenBuilding/HelpersSetting", order = 1)]
public class HelpersSetting : ScriptableObject
{
  public GameObject sizeHelper;
  public GameObject prefabHelper;

  public Material InvisibleVoid;
  public Material LastFloorDeco;
  public Material FirstFloorDeco;
  public Material FirstFloorWallSpace;
  public Material LastFloorWallSpace;
  public Material WallSpace;
  public Material RoofSpace;
  public Material BalconyDeco;
  public Material StairsDeco;
  public Material DecalDeco;

}
