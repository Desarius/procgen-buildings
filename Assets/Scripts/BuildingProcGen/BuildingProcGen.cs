using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuildingProcGen : MonoBehaviour
{
    public HelpersSetting helpersSettings;
    public BuildingStyleSettings buildingSettings;
    private List<HelpersData> GeneratedHelpers;
    
    private HelpersData phd = null;
    
    [Range(1,20)]
    public int deep;
    [Range(1,20)]
    public int height;
    [Range(1,20)]
    public int prof;

    public Vector3 offset;
    private Vector3 maxposition = Vector3.zero; 
    private Vector3 minposition = Vector3.zero;
    
    public bool CutAngle_01;
    public bool CutAngle_02;
    public bool CutAngle_03;
    public bool CutAngle_04;

    public bool noWindow_SideA;
    public bool noWindow_SideB;
    public bool noWindow_SideC;
    public bool noWindow_SideD;
    
    [Space(10.0f)]
    
    public Slider deepSlider;
    public Slider profSlider;
    public Slider heightSlider;

    public TextMeshProUGUI deeptxt;
    public TextMeshProUGUI proftxt;
    public TextMeshProUGUI heighttxt;

    public Toggle cutcorn01;
    public Toggle cutcorn02;
    public Toggle cutcorn03;
    public Toggle cutcorn04;
    
    // Start is called before the first frame update
    void Start()
    {
        GeneratedHelpers = new List<HelpersData>();
        SetupType();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            Execute();
        }

        deep = (int)deepSlider.value;
        prof = (int)profSlider.value;
        height = (int)heightSlider.value;
        
        deeptxt.SetText(deepSlider.value.ToString());
        proftxt.SetText(profSlider.value.ToString());
        heighttxt.SetText( heightSlider.value.ToString());

        CutAngle_01 = cutcorn01.isOn;
        CutAngle_02 = cutcorn02.isOn;
        CutAngle_03 = cutcorn03.isOn;
        CutAngle_04 = cutcorn04.isOn;

    }

    public void Execute()
    {
        deep = (int)deepSlider.value;
        prof = (int)profSlider.value;
        height = (int)heightSlider.value;
        
        deeptxt.SetText(deepSlider.value.ToString());
        proftxt.SetText(profSlider.value.ToString());
        heighttxt.SetText( heightSlider.value.ToString());

        CutAngle_01 = cutcorn01.isOn;
        CutAngle_02 = cutcorn02.isOn;
        CutAngle_03 = cutcorn03.isOn;
        CutAngle_04 = cutcorn04.isOn;
        
        
        DestroyGeneratedHelpers();
        GenerateArrays();
        BuildHelpers();
    }

    public void GenerateArrays()
    {

    }
    
    public void BuildHelpers()
    {
        #region CornersData

        int xx = 0;
        int yy = 0;
        int zz = 0;
        int ww = 0;

        if (CutAngle_01) xx = 1;
        if (CutAngle_02) yy = 1;
        if (CutAngle_03) zz = 1;
        if (CutAngle_04) ww = 1;
        Vector4 corners = new Vector4(xx, yy, zz, ww);
        
        #endregion
        GeneratedHelpers = new List<HelpersData>();
        
        Mesh mesh = helpersSettings.sizeHelper.GetComponent<MeshFilter>().sharedMesh;
        Vector3 mSize = mesh.bounds.size;

        int cid = 0;

        //Generate Side01

        for (int x = 0; x < deep-1; x++)
        {
            for (int y = 0; y < height-1; y++)
            {
                int z = 0;
                cid++;
                GenerateHelpers(x, y, z, cid);

            }
        }
        
        //Generate Side02

        for (int y = 0; y < height-1; y++)
        {
            for (int z = 1; z < prof-1; z++)
            {
                int x = 0;
                cid++;
                GenerateHelpers(x, y, z, cid);
            }
        }
        
        //Generate Side03

        for (int x = 0; x < deep-1; x++)
        {
            for (int y = 0; y < height-1; y++)
            {
                int z = prof - 1;
                cid++;
                GenerateHelpers(x, y, z, cid);
            }
        }
        
        //Generate Side04

        for (int y = 0; y < height-1; y++)
        {
            for (int z = 0; z < prof; z++)
            {
                int x = deep - 1;
                cid++;
                GenerateHelpers(x,y,z, cid);
            }
        }
        
        //Generate Roof
        for (int x = 0; x < deep; x++)
        {
            for (int z = 0; z < prof; z++)
            {
                int y = height - 1;
                cid++;
                GenerateHelpers(x, y, z, cid);
            }
        }
        
        DecorateChainFirstPass(mSize, corners);

        maxposition = new Vector3(mSize.x * deep, 0.035f, mSize.z * prof);
        Vector3 midpoint = maxposition * 0.5f;
        offset = new Vector3((midpoint.x), 0.035f, (midpoint.z));
       //transform.position = new Vector3(offset.x, 0.035f, offset.z);


    }

    private void GenerateHelpers(int k, int j, int i, int id)
    {
        Mesh mesh = helpersSettings.sizeHelper.GetComponent<MeshFilter>().sharedMesh;
        Vector3 mSize = mesh.bounds.size;
        //Vector3 currposition = this.transform.position;
        Vector3 currposition = new Vector3(0, 0, 0);

        int cid = id;
        
        //if (CheckVisibility(k,j,i ))
          //          {
                        
                       // currposition.x = transform.position.x + ((k) * mSize.x);
                       // currposition.y = transform.position.y + ((j) * mSize.y);
                       // currposition.z = transform.position.z + ((i) * mSize.z);
                        
                        currposition.x = currposition.x + ((k) * mSize.x) - offset.x;
                        currposition.y = currposition.y + ((j) * mSize.y) + offset.y;
                        currposition.z = currposition.z + ((i) * mSize.z) - offset.z;

                        cid++;

                        GameObject xo = Instantiate(helpersSettings.prefabHelper, currposition, Quaternion.identity, this.transform);
                        xo.name = "Box_" + k + "_" + j + "_" + i;

                        HelpersData hp = xo.GetComponent<HelpersData>();
                        hp.go = xo;
                        hp.id = cid;
                        
                        hp.position = currposition;
                        hp.x = k;
                        hp.y = j;
                        hp.z = i;
                        
                        hp.isVisible = DetectVisibility(hp);
                        hp.isFirstFloor = DetectFirstFloor(hp);
                        hp.isLastFloor = DetectLastFloor(hp);
                        hp.isCorner = DetectCorner(hp);
                        hp.isRoof = DetectRoof(hp);
                        hp.isSide01 = DetectSide01(hp);
                        hp.isSide02 = DetectSide02(hp);
                        hp.isSide03 = DetectSide03(hp);
                        hp.isSide04 = DetectSide04(hp);

                        if (phd == null)
                        {
                            hp.PrevHelpersData = null;
                            phd = hp;
                        }
                        else
                        {
                            hp.PrevHelpersData = phd;
                            phd = hp;
                        }

                        GeneratedHelpers.Add(hp);

                  //  }
                  //  else
                  //  {
                        
                  //  }
         
    }
    
    
    private bool DetectSide01(HelpersData hp)
    {
        if ((hp.x <= deep || hp.x >= 0) && hp.z == 0)
        {
            return true;
        }

        return false;
    }

    private bool DetectSide02(HelpersData hp)
    {
        if (hp.x == 0 && ( hp.z >= 0 || hp.z <= prof))
        {
            return true;
        }

        return false;
    }
    
    private bool DetectSide03(HelpersData hp)
    {
        if ((hp.x <= deep || hp.x >= 0) && hp.z == prof-1 )
        {
            return true;
        }

        return false;
    }
    
    private bool DetectSide04(HelpersData hp)
    {
        if (hp.x == deep-1 && ( hp.z >= 0 || hp.z <= prof))
        {
            return true;
        }

        return false;
    }
    
    private bool DetectCorner(HelpersData hp)
    {
        if (hp.x == 0 && hp.z == 0)
        {
            return true;
        }

        if (hp.x == 0 && hp.z == (prof - 1) )
        {
            return true;
        }

        if (hp.x == (deep - 1) && hp.z == (prof - 1) )
        {
            return true;
        }

        if (hp.x == (deep - 1) && hp.z == 0)
        {
            return true;
        }

        return false;

    }

    //WALLS 
    public void DecorateChainFirstPass(Vector3 size, Vector4 corners)
    {

        foreach (HelpersData helper in GeneratedHelpers)
        {

            buildingSettings.StyleRuleset.ProcGenRules(helper, corners, deep, height, prof );
          
            #region Walls Generation
            
            if (helper.wallType != WallType.Void)
            {
                
                if (!helper.isCorner & helper.isSide01)
                {
                    CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, -90, 0));
                }

                if (!helper.isCorner & helper.isSide02)
                {
                    CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 0, 0));
                }

                if (!helper.isCorner & helper.isSide03)
                {
                    CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 90, 0));
                }
                
                if (!helper.isCorner & helper.isSide04)
                {
                    CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 180, 0));
                }

                if (helper.isCorner)
                {
                    if (CutAngle_01)
                    {
                        if (helper.isSide01 & helper.isSide02)
                        {
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, -90, 0));
                        }
                    }
                    else if (!CutAngle_01)
                    {
                        if (helper.isSide01 & helper.isSide02)
                        {
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, -90, 0));
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, -0, 0));
                        }
                    }

                    if (CutAngle_02)
                    {
                        if (helper.isSide01 & helper.isSide04)
                        {
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 180, 0));
                        }
                    } else if (!CutAngle_02)
                    {
                        if (helper.isSide01 & helper.isSide04)
                        {
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 180, 0));
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 270, 0));
                        }
                    }

                    if (CutAngle_03)
                    {
                        if (helper.isSide03 & helper.isSide04)
                        {
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 90, 0));
                        }
                    } else if (!CutAngle_03)
                    {
                        if (helper.isSide03 & helper.isSide04)
                        {
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 90, 0));
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 180, 0));
                        }
                    }

                    if (CutAngle_04)
                    {
                        if (helper.isSide03 & helper.isSide02)
                        {
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 0, 0));
                        }
                    } else if (!CutAngle_04)
                    {
                        if (helper.isSide03 & helper.isSide02)
                        {
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 0, 0));
                            CreateGo(helper, helper.wallType, buildingSettings, Quaternion.Euler(0, 90, 0));
                        }
                    }
                }

            }
            
            #endregion
            
            #region Roof Generation
            
            if (helper.roofType != RoofType.Void)
            {
                
                if (!helper.isCorner & helper.isSide01 & !helper.isSide02 & !helper.isSide03 & !helper.isSide04)
                {
                    CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, -90, 0));
                }

                if (!helper.isCorner & helper.isSide02 & !helper.isSide01 & !helper.isSide03 & !helper.isSide04)
                {
                    CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 0, 0));
                }

                if (!helper.isCorner & helper.isSide03 & !helper.isSide02 & !helper.isSide04 & !helper.isSide01)
                {
                    CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 90, 0));
                }
                
                if (!helper.isCorner & helper.isSide04 & !helper.isSide01 & !helper.isSide02 & !helper.isSide03)
                {
                    CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 180, 0));
                }
                
                if (!helper.isCorner & helper.isRoof & !helper.isSide01 & !helper.isSide02  & !helper.isSide03  & !helper.isSide04 )
                {
                    CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 0, 0));
                }


                if (helper.isCorner)
                {
                    if (CutAngle_01)
                    {
                        if (helper.isSide01 & helper.isSide02)
                        {
                            CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, -90, 0));
                        }
                    }
                    else if (!CutAngle_01)
                    {
                        if (helper.isSide01 & helper.isSide02)
                        {
                            CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, -90, 0));
                            //CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, -0, 0));
                        }
                    }

                    if (CutAngle_02)
                    {
                        if (helper.isSide01 & helper.isSide04)
                        {
                            CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 180, 0));
                        }
                    } else if (!CutAngle_02)
                    {
                        if (helper.isSide01 & helper.isSide04)
                        {
                            CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 180, 0));
                            //CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 270, 0));
                        }
                    }

                    if (CutAngle_03)
                    {
                        if (helper.isSide03 & helper.isSide04)
                        {
                            CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 90, 0));
                        }
                    } else if (!CutAngle_03)
                    {
                        if (helper.isSide03 & helper.isSide04)
                        {
                            CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 90, 0));
                            //CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 180, 0));
                        }
                    }

                    if (CutAngle_04)
                    {
                        if (helper.isSide03 & helper.isSide02)
                        {
                            CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 0, 0));
                        }
                    } else if (!CutAngle_04)
                    {
                        if (helper.isSide03 & helper.isSide02)
                        {
                            CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 0, 0));
                            //CreateGo(helper, helper.roofType, buildingSettings, Quaternion.Euler(0, 90, 0));
                        }
                    }
                }

            }
            
            #endregion
            
            #region Decoration Generation
            
            if (helper.decoType != DecoType.Void)
            {
                
                if (!helper.isCorner & helper.isSide01)
                {
                    CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, -90, 0));
                }

                if (!helper.isCorner & helper.isSide02)
                {
                    CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 0, 0));
                }

                if (!helper.isCorner & helper.isSide03)
                {
                    CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 90, 0));
                }
                
                if (!helper.isCorner & helper.isSide04)
                {
                    CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 180, 0));
                }

                if (!helper.isCorner & helper.isRoof & !helper.isSide01 & !helper.isSide02  & !helper.isSide03  & !helper.isSide04  )
                {
                    CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 0, 0));
                }

                if (helper.isCorner)
                {
                    if (CutAngle_01)
                    {
                        if (helper.isSide01 & helper.isSide02)
                        {
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, -90, 0));
                        }
                    }
                    else if (!CutAngle_01)
                    {
                        if (helper.isSide01 & helper.isSide02)
                        {
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, -90, 0));
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, -0, 0));
                        }
                    }

                    if (CutAngle_02)
                    {
                        if (helper.isSide01 & helper.isSide04)
                        {
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 180, 0));
                        }
                    } else if (!CutAngle_02)
                    {
                        if (helper.isSide01 & helper.isSide04)
                        {
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 180, 0));
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 270, 0));
                        }
                    }

                    if (CutAngle_03)
                    {
                        if (helper.isSide03 & helper.isSide04)
                        {
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 90, 0));
                        }
                    } else if (!CutAngle_03)
                    {
                        if (helper.isSide03 & helper.isSide04)
                        {
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 90, 0));
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 180, 0));
                        }
                    }

                    if (CutAngle_04)
                    {
                        if (helper.isSide03 & helper.isSide02)
                        {
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 0, 0));
                        }
                    } else if (!CutAngle_04)
                    {
                        if (helper.isSide03 & helper.isSide02)
                        {
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 0, 0));
                            CreateGo(helper, helper.decoType, buildingSettings, Quaternion.Euler(0, 90, 0));
                        }
                    }
                }

            }
            
            #endregion
            
        }
      
        
        
    }

    
    private void CreateGo(HelpersData helper, WallType helperWallType, BuildingStyleSettings buildingStyleSettings, Quaternion rotation)
    {
        
        GameObject go = GetGOByWallType(helper.wallType, buildingSettings);
        if (go != null)
        {
            GameObject wall = Instantiate(go, helper.position, rotation, helper.transform);
            wall.name = helper.wallType.ToString();

            helper.wallGO.Add(wall);
        }
        
    }

    private void CreateGo(HelpersData helper, RoofType helperWallType, BuildingStyleSettings buildingStyleSettings, Quaternion rotation)
    {
        
        GameObject go = GetGOByWallType(helper.roofType, buildingSettings);
        if (go != null)
        {
            GameObject roof = Instantiate(go, helper.position, rotation, helper.transform);
            roof.name = helper.wallType.ToString();

            helper.roofGO.Add(roof);
        }
        
    }
    
    private void CreateGo(HelpersData helper, DecoType helperWallType, BuildingStyleSettings buildingStyleSettings, Quaternion rotation)
    {
        
        GameObject go = GetGOByWallType(helper.decoType, buildingSettings);
        if (go != null)
        {
            GameObject deco = Instantiate(go, helper.position, rotation, helper.transform);
            deco.name = helper.wallType.ToString();

            helper.roofGO.Add(deco);
        }
        
    }
    
    public GameObject GetGOByWallType(WallType wallType, BuildingStyleSettings bld)
    {
        switch (wallType)
        {
            case WallType.Void:
                return null;
                
            case WallType.Wall_Normal_Linear_00:
                if (bld.Wall_Normal_Linear_00.Count >= 1)
                {
                    return bld.Wall_Normal_Linear_00[Random.Range(0,bld.Wall_Normal_Linear_00.Count)];
                }
                else
                {
                    return null;
                }

            case WallType.Wall_Normal_Linear_01:
                if (bld.Wall_Normal_Linear_01.Count >= 1)
                {
                    return bld.Wall_Normal_Linear_01[Random.Range(0,bld.Wall_Normal_Linear_01.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Normal_Linear_02:
                if (bld.Wall_Normal_Linear_02.Count >= 1)
                {
                    return bld.Wall_Normal_Linear_02[Random.Range(0,bld.Wall_Normal_Linear_02.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Normal_Linear_03:
                if (bld.Wall_Normal_Linear_03.Count >= 1)
                {
                    return bld.Wall_Normal_Linear_03[Random.Range(0,bld.Wall_Normal_Linear_03.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Normal_Linear_04:
                if (bld.Wall_Normal_Linear_04.Count >= 1)
                {
                    return bld.Wall_Normal_Linear_04[Random.Range(0,bld.Wall_Normal_Linear_04.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Curved_Angle_00:
                if (bld.Wall_Curved_Angle_00.Count >= 1)
                {
                    return bld.Wall_Curved_Angle_00[Random.Range(0,bld.Wall_Curved_Angle_00.Count)];
                }
                else
                {
                    return null;
                }
                
            case WallType.Wall_Curved_Angle_01:
                if (bld.Wall_Curved_Angle_01.Count >= 1)
                {
                    return bld.Wall_Curved_Angle_01[Random.Range(0,bld.Wall_Curved_Angle_01.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Curved_Angle_02:
                if (bld.Wall_Curved_Angle_02.Count >= 1)
                {
                    return bld.Wall_Curved_Angle_02[Random.Range(0,bld.Wall_Curved_Angle_02.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Curved_Angle_03:
                if (bld.Wall_Curved_Angle_03.Count >= 1)
                {
                    return bld.Wall_Curved_Angle_03[Random.Range(0,bld.Wall_Curved_Angle_03.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Curved_Angle_04:
                if (bld.Wall_Curved_Angle_04.Count >= 1)
                {
                    return bld.Wall_Curved_Angle_04[Random.Range(0,bld.Wall_Curved_Angle_04.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Normal_Angle_00:
                if (bld.Wall_Normal_Angle_00.Count >= 1)
                {
                    return bld.Wall_Normal_Angle_00[Random.Range(0,bld.Wall_Normal_Angle_00.Count)];
                }
                else
                {
                    return null;
                }

            case WallType.Wall_Normal_Angle_01:
                if (bld.Wall_Normal_Angle_01.Count >= 1)
                {
                    return bld.Wall_Normal_Angle_01[Random.Range(0,bld.Wall_Normal_Angle_01.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Normal_Angle_02:
                if (bld.Wall_Normal_Angle_02.Count >= 1)
                {
                    return bld.Wall_Normal_Angle_02[Random.Range(0,bld.Wall_Normal_Angle_02.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Normal_Angle_03:
                if (bld.Wall_Normal_Angle_03.Count >= 1)
                {
                    return bld.Wall_Normal_Angle_03[Random.Range(0,bld.Wall_Normal_Angle_03.Count)];
                }
                else
                {
                    return null;
                }
            
            case WallType.Wall_Normal_Angle_04:
                if (bld.Wall_Normal_Angle_04.Count >= 1)
                {
                    return bld.Wall_Normal_Angle_04[Random.Range(0,bld.Wall_Normal_Angle_04.Count)];
                }
                else
                {
                    return null;
                }
            
            default:
                return null;
               
        }
    }

    public GameObject GetGOByWallType(RoofType roofType, BuildingStyleSettings bld)
    {
        switch (roofType)
        {
            case RoofType.Void:
                return null;
            
            case RoofType.Roof_Curved_Angle_00:
                if (bld.Roof_Curved_Angle_00.Count >= 1)
                {
                    return bld.Roof_Curved_Angle_00[Random.Range(0,bld.Roof_Curved_Angle_00.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Curved_Angle_01:
                if (bld.Roof_Curved_Angle_01.Count >= 1)
                {
                    return bld.Roof_Curved_Angle_01[Random.Range(0,bld.Roof_Curved_Angle_01.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Curved_Angle_02:
                if (bld.Roof_Curved_Angle_02.Count >= 1)
                {
                    return bld.Roof_Curved_Angle_02[Random.Range(0,bld.Roof_Curved_Angle_02.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Curved_Angle_03:
                if (bld.Roof_Curved_Angle_03.Count >= 1)
                {
                    return bld.Roof_Curved_Angle_03[Random.Range(0,bld.Roof_Curved_Angle_03.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Curved_Angle_04:
                if (bld.Roof_Curved_Angle_04.Count >= 1)
                {
                    return bld.Roof_Curved_Angle_04[Random.Range(0,bld.Roof_Curved_Angle_04.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Normal_Linear_00:
                if (bld.Roof_Normal_Linear_00.Count >= 1)
                {
                    return bld.Roof_Normal_Linear_00[Random.Range(0,bld.Roof_Normal_Linear_00.Count)];
                }
                else
                {
                    return null;
                }
            
                        
            case RoofType.Roof_Normal_Linear_01:
                if (bld.Roof_Normal_Linear_01.Count >= 1)
                {
                    return bld.Roof_Normal_Linear_01[Random.Range(0,bld.Roof_Normal_Linear_01.Count)];
                }
                else
                {
                    return null;
                }
            
                        
            case RoofType.Roof_Normal_Linear_02:
                if (bld.Roof_Normal_Linear_02.Count >= 1)
                {
                    return bld.Roof_Normal_Linear_02[Random.Range(0,bld.Roof_Normal_Linear_02.Count)];
                }
                else
                {
                    return null;
                }
            
                        
            case RoofType.Roof_Normal_Linear_03:
                if (bld.Roof_Normal_Linear_03.Count >= 1)
                {
                    return bld.Roof_Normal_Linear_03[Random.Range(0,bld.Roof_Normal_Linear_03.Count)];
                }
                else
                {
                    return null;
                }
            
                        
            case RoofType.Roof_Normal_Linear_04:
                if (bld.Roof_Normal_Linear_04.Count >= 1)
                {
                    return bld.Roof_Normal_Linear_04[Random.Range(0,bld.Roof_Normal_Linear_04.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Curved_Curved_00:
                if (bld.Roof_Curved_Curved_00.Count >= 1)
                {
                    return bld.Roof_Curved_Curved_00[Random.Range(0,bld.Roof_Curved_Curved_00.Count)];
                }
                else
                {
                    return null;
                }
                        
            case RoofType.Roof_Curved_Curved_01:
                if (bld.Roof_Curved_Curved_01.Count >= 1)
                {
                    return bld.Roof_Curved_Curved_01[Random.Range(0,bld.Roof_Curved_Curved_01.Count)];
                }
                else
                {
                    return null;
                }
                        
            case RoofType.Roof_Curved_Curved_02:
                if (bld.Roof_Curved_Curved_02.Count >= 1)
                {
                    return bld.Roof_Curved_Curved_02[Random.Range(0,bld.Roof_Curved_Curved_02.Count)];
                }
                else
                {
                    return null;
                }
                        
            case RoofType.Roof_Curved_Curved_03:
                if (bld.Roof_Curved_Curved_03.Count >= 1)
                {
                    return bld.Roof_Curved_Curved_03[Random.Range(0,bld.Roof_Curved_Curved_03.Count)];
                }
                else
                {
                    return null;
                }
                        
            case RoofType.Roof_Curved_Curved_04:
                if (bld.Roof_Curved_Curved_04.Count >= 1)
                {
                    return bld.Roof_Curved_Curved_04[Random.Range(0,bld.Roof_Curved_Curved_04.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Normal_Angle_00:
                if (bld.Roof_Normal_Angle_00.Count >= 1)
                {
                    return bld.Roof_Normal_Angle_00[Random.Range(0,bld.Roof_Normal_Angle_00.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Normal_Angle_01:
                if (bld.Roof_Normal_Angle_01.Count >= 1)
                {
                    return bld.Roof_Normal_Angle_01[Random.Range(0,bld.Roof_Normal_Angle_01.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Normal_Angle_02:
                if (bld.Roof_Normal_Angle_02.Count >= 1)
                {
                    return bld.Roof_Normal_Angle_02[Random.Range(0,bld.Roof_Normal_Angle_02.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Normal_Angle_03:
                if (bld.Roof_Normal_Angle_03.Count >= 1)
                {
                    return bld.Roof_Normal_Angle_03[Random.Range(0,bld.Roof_Normal_Angle_03.Count)];
                }
                else
                {
                    return null;
                }
            
            case RoofType.Roof_Normal_Angle_04:
                if (bld.Roof_Normal_Angle_04.Count >= 1)
                {
                    return bld.Roof_Normal_Angle_04[Random.Range(0,bld.Roof_Normal_Angle_04.Count)];
                }
                else
                {
                    return null;
                }
            
            
            default:
                return null;
        }
        
    }

    public GameObject GetGOByWallType(DecoType decoType, BuildingStyleSettings bld)
    {
        switch (decoType)
        {
            case DecoType.Void:
                return null;
            
            
            case DecoType.Deco_Curved_Angle_00:
                if (bld.Deco_Curved_Angle_00.Count >= 1)
                {
                    return bld.Deco_Curved_Angle_00[Random.Range(0,bld.Deco_Curved_Angle_00.Count)];
                }
                else
                {
                    return null;
                }
                        
            case DecoType.Deco_Curved_Angle_01:
                if (bld.Deco_Curved_Angle_01.Count >= 1)
                {
                    return bld.Deco_Curved_Angle_01[Random.Range(0,bld.Deco_Curved_Angle_01.Count)];
                }
                else
                {
                    return null;
                }
                        
            case DecoType.Deco_Curved_Angle_02:
                if (bld.Deco_Curved_Angle_02.Count >= 1)
                {
                    return bld.Deco_Curved_Angle_02[Random.Range(0,bld.Deco_Curved_Angle_02.Count)];
                }
                else
                {
                    return null;
                }
                        
            case DecoType.Deco_Curved_Angle_03:
                if (bld.Deco_Curved_Angle_03.Count >= 1)
                {
                    return bld.Deco_Curved_Angle_03[Random.Range(0,bld.Deco_Curved_Angle_03.Count)];
                }
                else
                {
                    return null;
                }
                        
            case DecoType.Deco_Curved_Angle_04:
                if (bld.Deco_Curved_Angle_04.Count >= 1)
                {
                    return bld.Deco_Curved_Angle_04[Random.Range(0,bld.Deco_Curved_Angle_04.Count)];
                }
                else
                {
                    return null;
                }
            
            case DecoType.Deco_Normal_Linear_00:
                if (bld.Deco_Normal_Linear_00.Count >= 1)
                {
                    return bld.Deco_Normal_Linear_00[Random.Range(0,bld.Deco_Normal_Linear_00.Count)];
                }
                else
                {
                    return null;
                }
            
            case DecoType.Deco_Normal_Linear_01:
                if (bld.Deco_Normal_Linear_01.Count >= 1)
                {
                    return bld.Deco_Normal_Linear_01[Random.Range(0,bld.Deco_Normal_Linear_01.Count)];
                }
                else
                {
                    return null;
                }
            
            case DecoType.Deco_Normal_Linear_02:
                if (bld.Deco_Normal_Linear_02.Count >= 1)
                {
                    return bld.Deco_Normal_Linear_02[Random.Range(0,bld.Deco_Normal_Linear_02.Count)];
                }
                else
                {
                    return null;
                }
            
            case DecoType.Deco_Normal_Linear_03:
                if (bld.Deco_Normal_Linear_03.Count >= 1)
                {
                    return bld.Deco_Normal_Linear_03[Random.Range(0,bld.Deco_Normal_Linear_03.Count)];
                }
                else
                {
                    return null;
                }
            
            case DecoType.Deco_Normal_Linear_04:
                if (bld.Deco_Normal_Linear_04.Count >= 1)
                {
                    return bld.Deco_Normal_Linear_04[Random.Range(0,bld.Deco_Normal_Linear_04.Count)];
                }
                else
                {
                    return null;
                }
            
            case DecoType.Deco_Normal_Angle_00:
                if (bld.Deco_Normal_Angle_00.Count >= 1)
                {
                    return bld.Deco_Normal_Angle_00[Random.Range(0,bld.Deco_Normal_Angle_00.Count)];
                }
                else
                {
                    return null;
                }
                        
            case DecoType.Deco_Normal_Angle_01:
                if (bld.Deco_Normal_Angle_01.Count >= 1)
                {
                    return bld.Deco_Normal_Angle_01[Random.Range(0,bld.Deco_Normal_Angle_01.Count)];
                }
                else
                {
                    return null;
                }
                        
            case DecoType.Deco_Normal_Angle_02:
                if (bld.Deco_Normal_Angle_02.Count >= 1)
                {
                    return bld.Deco_Normal_Angle_02[Random.Range(0,bld.Deco_Normal_Angle_02.Count)];
                }
                else
                {
                    return null;
                }
                        
            case DecoType.Deco_Normal_Angle_03:
                if (bld.Deco_Normal_Angle_03.Count >= 1)
                {
                    return bld.Deco_Normal_Angle_03[Random.Range(0,bld.Deco_Normal_Angle_03.Count)];
                }
                else
                {
                    return null;
                }
                        
            case DecoType.Deco_Normal_Angle_04:
                if (bld.Deco_Normal_Angle_04.Count >= 1)
                {
                    return bld.Deco_Normal_Angle_04[Random.Range(0,bld.Deco_Normal_Angle_04.Count)];
                }
                else
                {
                    return null;
                }
                     
            case DecoType.Deco_Normal_Roof_00:
                if (bld.Deco_Normal_Roof_00.Count >= 1)
                {
                    return bld.Deco_Normal_Roof_00[Random.Range(0,bld.Deco_Normal_Roof_00.Count)];
                }
                else
                {
                    return null;
                }   
                                 
            case DecoType.Deco_Normal_Roof_01:
                if (bld.Deco_Normal_Roof_01.Count >= 1)
                {
                    return bld.Deco_Normal_Roof_01[Random.Range(0,bld.Deco_Normal_Roof_01.Count)];
                }
                else
                {
                    return null;
                }   
                                 
            case DecoType.Deco_Normal_Roof_02:
                if (bld.Deco_Normal_Roof_02.Count >= 1)
                {
                    return bld.Deco_Normal_Roof_02[Random.Range(0,bld.Deco_Normal_Roof_02.Count)];
                }
                else
                {
                    return null;
                }   
                                 
            case DecoType.Deco_Normal_Roof_03:
                if (bld.Deco_Normal_Roof_03.Count >= 1)
                {
                    return bld.Deco_Normal_Roof_03[Random.Range(0,bld.Deco_Normal_Roof_03.Count)];
                }
                else
                {
                    return null;
                }   
                                 
            case DecoType.Deco_Normal_Roof_04:
                if (bld.Deco_Normal_Roof_04.Count >= 1)
                {
                    return bld.Deco_Normal_Roof_04[Random.Range(0,bld.Deco_Normal_Roof_04.Count)];
                }
                else
                {
                    return null;
                }   
            
            case DecoType.Deco_Curved_Roof_00:
                if (bld.Deco_Curved_Roof_00.Count >= 1)
                {
                    return bld.Deco_Curved_Roof_00[Random.Range(0,bld.Deco_Curved_Roof_00.Count)];
                }
                else
                {
                    return null;
                } 
                        
            case DecoType.Deco_Curved_Roof_01:
                if (bld.Deco_Curved_Roof_01.Count >= 1)
                {
                    return bld.Deco_Curved_Roof_01[Random.Range(0,bld.Deco_Curved_Roof_01.Count)];
                }
                else
                {
                    return null;
                } 
                        
            case DecoType.Deco_Curved_Roof_02:
                if (bld.Deco_Curved_Roof_02.Count >= 1)
                {
                    return bld.Deco_Curved_Roof_02[Random.Range(0,bld.Deco_Curved_Roof_02.Count)];
                }
                else
                {
                    return null;
                } 
                        
            case DecoType.Deco_Curved_Roof_03:
                if (bld.Deco_Curved_Roof_03.Count >= 1)
                {
                    return bld.Deco_Curved_Roof_03[Random.Range(0,bld.Deco_Curved_Roof_03.Count)];
                }
                else
                {
                    return null;
                } 
                        
            case DecoType.Deco_Curved_Roof_04:
                if (bld.Deco_Curved_Roof_04.Count >= 1)
                {
                    return bld.Deco_Curved_Roof_04[Random.Range(0,bld.Deco_Curved_Roof_04.Count)];
                }
                else
                {
                    return null;
                } 
            
            default:
                return null;
        }
        
    }
    
    
    public HelpersData MakeChainsfromEnd(HelpersData endhelpersData)
    {
        if (endhelpersData.PrevHelpersData != null)
        {
            endhelpersData.PrevHelpersData.NextHelpersData = endhelpersData;
            
            return MakeChainsfromEnd(endhelpersData.PrevHelpersData);
        }
        else
        {
            return null;
        }
    }

    public bool CheckVisibility(int x, int y, int z)
    {
        if (x == 0 || x == (deep - 1))
        {
            return true;
        }

        if (z == 0 || z == (prof - 1))
        {
            return true;
        }

        if (y == (height - 1))
        {
            return true;
        }
        
        return false;
    }

    public bool DetectRoof(int x, int y, int z)
    {
        if (y == (height - 1))
        {
            return true;
        }
        
        return false;
    }

    public bool DetectRoof(HelpersData helpersData)
    {
        if (helpersData.y == (height - 1))
        {
            return true;
        }
        
        return false;
    }
    
    
    public bool DetectVisibility(HelpersData helpersData)
    {
        if (helpersData.x == 0 || helpersData.x == deep -1)
        {
            return true;
        }

        if (helpersData.z == 0 || helpersData.z == prof -1)
        {
            return true;
        }
        
        return false;
    }

    public bool DetectFirstFloor(HelpersData helpersData)
    {
        if (helpersData.y == 1)
        {
            return true;
        }

        return false;
    }

    public bool DetectLastFloor(HelpersData helpersData)
    {
        if (helpersData.y == height)
        {
            return true;
        }

        return false;
    }
    
    public void DestroyGeneratedHelpers()
    {
        if (GeneratedHelpers.Count >= 1)
        {
            //For each helpers data
            foreach (HelpersData hd in GeneratedHelpers )
            {
                //Destroy Walls
                foreach (GameObject go in hd.wallGO)
                {
                    Destroy(go);
                }
                
                //Destroy Decorations
                foreach (GameObject go in hd.decoGO)
                {
                    Destroy(go);
                }
                
                //Destroy HelpersCube
                Destroy(hd.go);
            }
            
        }

    }

    public void SetupType()
    {
        switch (buildingSettings.styleType)
        {
            case StyleType.Base:
                //BaseStyle baseStyle = new BaseStyle();
                BaseStyle baseStyle = ScriptableObject.CreateInstance<BaseStyle>();
                buildingSettings.StyleRuleset = baseStyle;
                break;
            
            case StyleType.Advanced:
                //AdvancedStyle advancedStyle = new AdvancedStyle();
                AdvancedStyle advancedStyle = ScriptableObject.CreateInstance<AdvancedStyle>();
                buildingSettings.StyleRuleset = advancedStyle;
                break;
            
        }
    }
    
    
}
