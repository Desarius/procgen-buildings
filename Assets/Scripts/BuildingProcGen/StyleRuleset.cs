using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public enum WallType
{
    Void,
    Wall_Normal_Linear_00,
    Wall_Normal_Linear_01,
    Wall_Normal_Linear_02,
    Wall_Normal_Linear_03,
    Wall_Normal_Linear_04,
    Wall_Curved_Angle_00,
    Wall_Curved_Angle_01,
    Wall_Curved_Angle_02,
    Wall_Curved_Angle_03,
    Wall_Curved_Angle_04,
    Wall_Normal_Angle_00,
    Wall_Normal_Angle_01,
    Wall_Normal_Angle_02,
    Wall_Normal_Angle_03,
    Wall_Normal_Angle_04
    
}

public enum RoofType
{
    Void,
    Roof_Normal_Linear_00,
    Roof_Normal_Linear_01,
    Roof_Normal_Linear_02,
    Roof_Normal_Linear_03,
    Roof_Normal_Linear_04,
    Roof_Curved_Curved_00,
    Roof_Curved_Curved_01,
    Roof_Curved_Curved_02,
    Roof_Curved_Curved_03,
    Roof_Curved_Curved_04,
    Roof_Normal_Angle_01,
    Roof_Normal_Angle_02,
    Roof_Normal_Angle_03,
    Roof_Normal_Angle_04,
    Roof_Normal_Angle_00,
    Roof_Curved_Angle_00,
    Roof_Curved_Angle_01,
    Roof_Curved_Angle_02,
    Roof_Curved_Angle_03,
    Roof_Curved_Angle_04

}

public enum DecoType
{
    Void,
    Deco_Normal_Linear_00,
    Deco_Normal_Linear_01,
    Deco_Normal_Linear_02,
    Deco_Normal_Linear_03,
    Deco_Normal_Linear_04,
    Deco_Curved_Angle_00,
    Deco_Curved_Angle_01,
    Deco_Curved_Angle_02,
    Deco_Curved_Angle_03,
    Deco_Curved_Angle_04,
    Deco_Normal_Angle_00,
    Deco_Normal_Angle_01,
    Deco_Normal_Angle_02,
    Deco_Normal_Angle_03,
    Deco_Normal_Angle_04,
    Deco_Normal_Roof_00,
    Deco_Normal_Roof_01,
    Deco_Normal_Roof_02,
    Deco_Normal_Roof_03,
    Deco_Normal_Roof_04,
    Deco_Curved_Roof_00,
    Deco_Curved_Roof_01,
    Deco_Curved_Roof_02,
    Deco_Curved_Roof_03,
    Deco_Curved_Roof_04
}


public interface IStyleRuleset
{

    public void ProcGenRules(HelpersData helper, Vector4 corners,int x, int y, int z);

}
