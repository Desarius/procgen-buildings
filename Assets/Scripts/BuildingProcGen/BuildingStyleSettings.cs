using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public enum StyleType
{
    Base = 01,
    Advanced = 02
    
}


[CreateAssetMenu(fileName = "BuildingStyleSettings", menuName = "ProcGenBuilding/BuildingStyleSettings", order = 2)]
public class BuildingStyleSettings : ScriptableObject
{

    public List<GameObject> Roof_Normal_Linear_00;
    public List<GameObject> Roof_Normal_Linear_01;
    public List<GameObject> Roof_Normal_Linear_02;
    public List<GameObject> Roof_Normal_Linear_03;
    public List<GameObject> Roof_Normal_Linear_04;
   
    public List<GameObject> Roof_Curved_Curved_00;
    public List<GameObject> Roof_Curved_Curved_01;
    public List<GameObject> Roof_Curved_Curved_02;
    public List<GameObject> Roof_Curved_Curved_03;
    public List<GameObject> Roof_Curved_Curved_04;
    
    public List<GameObject> Roof_Normal_Angle_00;
    public List<GameObject> Roof_Normal_Angle_01;
    public List<GameObject> Roof_Normal_Angle_02;
    public List<GameObject> Roof_Normal_Angle_03;
    public List<GameObject> Roof_Normal_Angle_04;
    
    public List<GameObject> Roof_Curved_Angle_00;
    public List<GameObject> Roof_Curved_Angle_01;
    public List<GameObject> Roof_Curved_Angle_02;
    public List<GameObject> Roof_Curved_Angle_03;
    public List<GameObject> Roof_Curved_Angle_04;

    public List<GameObject> Wall_Normal_Linear_00;
    public List<GameObject> Wall_Normal_Linear_01;
    public List<GameObject> Wall_Normal_Linear_02;
    public List<GameObject> Wall_Normal_Linear_03;
    public List<GameObject> Wall_Normal_Linear_04;
    
    public List<GameObject> Wall_Curved_Angle_00;
    public List<GameObject> Wall_Curved_Angle_01;
    public List<GameObject> Wall_Curved_Angle_02;
    public List<GameObject> Wall_Curved_Angle_03;
    public List<GameObject> Wall_Curved_Angle_04;
    
    public List<GameObject> Wall_Normal_Angle_00;
    public List<GameObject> Wall_Normal_Angle_01;
    public List<GameObject> Wall_Normal_Angle_02;
    public List<GameObject> Wall_Normal_Angle_03;
    public List<GameObject> Wall_Normal_Angle_04;

    public List<GameObject> Deco_Normal_Linear_00;
    public List<GameObject> Deco_Normal_Linear_01;
    public List<GameObject> Deco_Normal_Linear_02;
    public List<GameObject> Deco_Normal_Linear_03;
    public List<GameObject> Deco_Normal_Linear_04;

    public List<GameObject> Deco_Curved_Angle_00;
    public List<GameObject> Deco_Curved_Angle_01;
    public List<GameObject> Deco_Curved_Angle_02;
    public List<GameObject> Deco_Curved_Angle_03;
    public List<GameObject> Deco_Curved_Angle_04;
    
    public List<GameObject> Deco_Normal_Angle_00;
    public List<GameObject> Deco_Normal_Angle_01;
    public List<GameObject> Deco_Normal_Angle_02;
    public List<GameObject> Deco_Normal_Angle_03;
    public List<GameObject> Deco_Normal_Angle_04;

    public List<GameObject> Deco_Normal_Roof_00;
    public List<GameObject> Deco_Normal_Roof_01;
    public List<GameObject> Deco_Normal_Roof_02;
    public List<GameObject> Deco_Normal_Roof_03;
    public List<GameObject> Deco_Normal_Roof_04;
    
    public List<GameObject> Deco_Curved_Roof_00;
    public List<GameObject> Deco_Curved_Roof_01;
    public List<GameObject> Deco_Curved_Roof_02;
    public List<GameObject> Deco_Curved_Roof_03;
    public List<GameObject> Deco_Curved_Roof_04;
    
    public IStyleRuleset StyleRuleset;
    public StyleType styleType;
    
}
