using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DecorationType
{
    Void,
    Wall,
    Door,
    Window,
    Roof,
    cutWall,
    cutDoor,
    cutWindow
}
public class DecorationData : MonoBehaviour
{
    
    public DecorationType decorationType;
    
    
}
