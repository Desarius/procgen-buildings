using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStyle : ScriptableObject,IStyleRuleset
{
    
    public void ProcGenRules(HelpersData helper, Vector4 corners, int x, int y, int z)
    {
       
       //Difference between cutted angle e normal angle
           if (corners.x == 1 & helper.x == 0 & helper.z == 0 )
           {
               helper.wallType = WallType.Wall_Curved_Angle_00;
               
           } else if (corners.y == 1 & helper.x == (x-1) & helper.z == 0)
           {
               helper.wallType = WallType.Wall_Curved_Angle_00;
               
           } else if (corners.z == 1 & helper.x == (x-1) & helper.z == (z-1))
           {
               helper.wallType = WallType.Wall_Curved_Angle_00;
               
           } else if (corners.w == 1 & helper.x == 0 & helper.z == (z-1))
           {
               helper.wallType = WallType.Wall_Curved_Angle_00;
               
           } else
           {
               helper.wallType = WallType.Wall_Normal_Linear_00;
               
           }

           if (helper.isRoof)
           {
               //Curved or normal roof 
               if (corners.x == 1 & helper.isCorner & helper.isSide01 & helper.isSide02)
               {
                   helper.roofType = RoofType.Roof_Curved_Angle_00;
               }
               else if (corners.x == 0 & helper.isCorner & helper.isSide01 & helper.isSide02)
               {
                   helper.roofType = RoofType.Roof_Normal_Angle_00;
               }
               else if (corners.y == 1 & helper.isCorner & helper.isSide01 & helper.isSide04)
               {
                   helper.roofType = RoofType.Roof_Curved_Angle_00;
               }
               else if (corners.y == 0 & helper.isCorner & helper.isSide01 & helper.isSide04)
               {
                   helper.roofType = RoofType.Roof_Normal_Angle_00;
               }
               else if (corners.z == 1 & helper.isCorner & helper.isSide03 & helper.isSide04)
               {
                   helper.roofType = RoofType.Roof_Curved_Angle_00;
               }
               else if (corners.z == 0 & helper.isCorner & helper.isSide03 & helper.isSide04)
               {
                   helper.roofType = RoofType.Roof_Normal_Angle_00;
               }
               else if (corners.w == 1  & helper.isCorner & helper.isSide03 & helper.isSide02)
               {
                   helper.roofType = RoofType.Roof_Curved_Angle_00;
               }
               else if (corners.w == 0  & helper.isCorner & helper.isSide03 & helper.isSide02)
               {
                   helper.roofType = RoofType.Roof_Normal_Angle_00;
               }
               else if ( !helper.isCorner && ( helper.isSide01 | helper.isSide02 | helper.isSide03 | helper.isSide04))
               {
                   helper.roofType = RoofType.Roof_Normal_Linear_01;
               }
               else
               {
                   helper.roofType = RoofType.Roof_Normal_Linear_00;
               }
           }


    }
    
    public bool CheckVisibility(int x, int y, int z, int k, int j , int i)
    {
        if (x == 0 || x == (k-1))
        {
            return true;
        }

        if (z == 0 || z == (i-1))
        {
            return true;
        }

        if (y == (j-1))
        {
            return true;
        }
        
        return false;
    }
    
    
}

public class AdvancedStyle : ScriptableObject,IStyleRuleset
{
    
    public void ProcGenRules(HelpersData helper,Vector4 corners,  int x, int y, int z)
    {
  
    }
   



}
