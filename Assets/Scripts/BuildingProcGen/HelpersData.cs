using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpersData : MonoBehaviour
{
    public int id;
    
    public GameObject go;
    public List<GameObject> wallGO;
    public List<GameObject> roofGO;
    public List<GameObject> decoGO;
    
    
    public Vector3 position;
    public int x;
    public int y;
    public int z;
    
    
    public bool isBorder;
    public bool isDecoSpace;
    public bool isVisible;
    public bool isGenerated;
    public bool isFirstFloor;
    public bool isLastFloor;
    public bool isCorner;
    public bool isRoof;
    public bool isSide01;
    public bool isSide02;
    public bool isSide03;
    public bool isSide04;
    
    
    public bool firstPassAlreadyDone;
    public bool secondPassAlreadyDone;
    public bool thirdPassAlreadyDone;
    
    public HelpersData NextHelpersData;
    public HelpersData PrevHelpersData;

    public WallType wallType = WallType.Void;
    public RoofType roofType = RoofType.Void;
    public DecoType decoType = DecoType.Void;
    
    
    public HelpersData()
    {
        
        wallGO = new List<GameObject>();
        roofGO = new List<GameObject>();
        decoGO = new List<GameObject>();
        
    }
    
    
    
}
